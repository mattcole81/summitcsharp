namespace LessonFive
{
    public abstract class SimpleUnderwriting
    {
        public abstract bool IsAcceptableDriver(IDriver driver);
    }
}