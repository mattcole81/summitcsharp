﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LessonFive
{
    class Program
    {

        /*
         * 1. Derive a policyholder class using the abstract base class Person (Person.cs)
         * 1.1 Ouput to the console the policyholders
         *    - Name
         *    - Date of birth
         * 
         * 2. Implement the IPolicyholder interface (IPolicyholder.cs) using the class created in 1.
         * 2.1 Ouput to the console the policyholders 
         *    - Address
         *    - Mobile Number
         *
         * 3. Update the interface called IDriver (IDriver.cs) with the following
         *    - property to capture the licence - typed LicenceType
         *    - property to capture the date the driver started driving - typed DateTime
         *    - a parameterless method named IsADriver which returns a boolean value
         *    - a parameterless method named GetAgeOfDriver which returns an integer value
         *    - a parameterless method named GetNumberOfYearsDriving which returns an integer value
         *  
         * 4. Implement the IDriver interface using the class modified in 2.
         * 4.1 The method IsADriver should verify that the person is a driver
         * 4.2 The method GetAgeOfDriver must calculate the age of the driver
         * 4.3 The method GetNumberOfYearsDriving must calculate the number of years since the driver started driving
         * 4.4 Output to the console the
         *    - result of IsADriver
         *    - result of GetAgeOfDriver
         *    - result of GetNumberOfYearsDriving
         * 
         * 5. Derive new class DriverUnderwiting using the abstract class SimpleUnderwriting (SimpleUnderwriting.cs)
         * 5.1 Implement (override) the method IsAcceptableDriver in the new class with the following rules
         *    - An acceptable driver must be a driver type
         *    - An acceptable driver is over the age of 25
         *    - An acceptable driver has an open licence
         *    - An acceptable driver has a pplater licence and has been driving for at least 2 years
         *    - An acceptable driver is not from a suburb in the range 4500 to 4600
         *    
         * 5.2 Overload the IsAcceptableDriver, adding an out parameter named "message" of type string to the method
         *    - the parameter "message" will return a reason that explains why result of the function is false or blank if the result is true
         */

        static void Main(string[] args)
        {

            var policyholder = new PolicyHolder();

            policyholder.FirstName = "Joe";
            policyholder.Surname = "Blog";

            // 1.
            Console.WriteLine("Name           : {0} {1}", policyholder.FirstName, policyholder.Surname);
            Console.WriteLine("Date of Birth  : {0}", policyholder.DateOfBirth.ToString("D"));
            //
            //            policyholder.SetAddress(new StreetAddress()
            //            {
            //                PostCode = 1234,
            //                Line1 = "10 Somplace Street",
            //                Suburb = "Suburbia"
            //            });
            //            policyholder.MobileNumber = "0411111111";
            //
            //            // 2.
            //            Console.WriteLine("");
            //            Console.WriteLine("Address        : {0}", policyholder.GetAddress());
            //            Console.WriteLine("Mobile         : {0}", policyholder.MobileNumber);
            //
            //            //4.
            //            Console.WriteLine("");
            //            Console.WriteLine("Is a driver    : {0}", policyholder.IsADriver());
            //            Console.WriteLine("Age of driver  : {0}", policyholder.GetAgeOfDriver());
            //            Console.WriteLine("Driving for    : {0} years", policyholder.GetAgeOfDriver());
            //
            //            policyholder.AddPersonType(PersonType.Driver);
            //            policyholder.LicenceType = LicenceType.International;
            //            policyholder.StartedDrivingDateTime = DateTime.Now;
            //
            //            //4.
            //            Console.WriteLine("");
            //            Console.WriteLine("Is a driver    : {0}", policyholder.IsADriver());
            //            Console.WriteLine("Age of driver  : {0}", policyholder.GetAgeOfDriver());
            //            Console.WriteLine("Driving for    : {0} years", policyholder.GetAgeOfDriver());
            //
            //            //5.
            //            var driverUnderwriting = new DriverUnderwriting();
            //            var isAcceptable = driverUnderwriting.IsAcceptableDriver(policyholder);
            //            Console.WriteLine("");
            //            Console.WriteLine("Is Acceptable  : {0}", isAcceptable);
            //
            //            string message;
            //            isAcceptable = driverUnderwriting.IsAcceptableDriver(policyholder, out message);
            //            Console.WriteLine("Is Acceptable  : {0} - {1}", isAcceptable, message);

            Console.WriteLine("");
            Console.WriteLine("Complete");
            Console.ReadKey();
        }
    }
}
