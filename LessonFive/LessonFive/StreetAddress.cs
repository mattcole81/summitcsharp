﻿namespace LessonFive
{
    public class StreetAddress
    {
        public string Line1 { get; set; }
        public string Suburb { get; set; }
        public int PostCode { get; set; }
    }
}