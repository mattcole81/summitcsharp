using System;
using System.Collections.Generic;

namespace LessonFive
{
    public abstract class Person
    {
        protected Person(PersonType personType)
        {
            PersonTypes = new List<PersonType>()
            {
                personType
            };
        }

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public List<PersonType> PersonTypes { get; }

        public void AddPersonType(PersonType personType)
        {
            PersonTypes.Add(personType);
        }
    }

    public class PolicyHolder : Person
    {
        public PolicyHolder() : base(PersonType.Policyholder)
        {
        }
    }
}