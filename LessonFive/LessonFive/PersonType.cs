namespace LessonFive
{
    public enum PersonType
    {
        Policyholder = 0,
        Driver = 1,
        Authorised = 2,
        Contact = 3
    }
}