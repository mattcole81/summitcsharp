namespace LessonFive
{
    public class PolicyHolder : Person, IPolicyholder
    {
        public PolicyHolder() : base(PersonType.Policyholder)
        {
        }

        public void SetAddress(StreetAddress address)
        {
            throw new System.NotImplementedException();
        }

        public string GetAddress()
        {
            throw new System.NotImplementedException();
        }

        public string MobileNumber { get; set; }
    }
}