namespace LessonFive
{
    public interface IPolicyholder
    {
        void SetAddress(StreetAddress address);
        string GetAddress();
        string MobileNumber { get; set; }
    }
}