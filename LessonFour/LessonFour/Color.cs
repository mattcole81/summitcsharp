using System;

namespace LessonFour
{
    class Color
    {
        public virtual void Fill()
        {
            Console.WriteLine("Fill me up with color");
        }
        public void Fill(string s)
        {
            Console.WriteLine("Fill me up with {0}", s);
        }
    };
}