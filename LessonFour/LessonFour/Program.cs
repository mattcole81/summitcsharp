﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonFour
{
    class Program
    {
        static void Main(string[] args)
        {
            var animal = new Animal();
            animal.Talk();
            animal.Sing();
            animal.Greet();

            var color = new Color();
            color.Fill();
            color.Fill("red");

            Console.WriteLine("");
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        /*
         1. Create a class called Dog that will derive from Animal that will orverride the Talk, Sing and Greet methods. These methods will output data to
         to the console that is specific to the Dog class.

         2. Create a class that will derive from Color which will output its colour to the console.

         3. Create a class that represents a Triangle, deriving from Shape and outputs the area of a triangle.
         */
    }
}
