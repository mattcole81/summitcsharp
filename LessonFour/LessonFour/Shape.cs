﻿using System;

namespace LessonFour
{
    class Shape
    {
        public double Width;
        public double Height;
        public void ShowDimension()
        {
            Console.WriteLine("Width and height are " +
                              Width + " and " + Height);
        }
    }
}