﻿using System.Collections.Generic;

namespace LessonNine.CSV
{
    public interface ICsvFileReader
    {
        List<VedaQueryDataItem> GetData(string filepath);
    }
}