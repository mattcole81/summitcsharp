using System;
using Autofac;
using LessonNine.CSV;

namespace LessonNine.Infrastructure
{
    public class AutofacInstaller
    {
        public static IContainer Container { get; private set; }

        public static void Register()
        {
            var builder = new Autofac.ContainerBuilder();

            builder.RegisterType<CsvFileReader>().As<ICsvFileReader>();

            Container = builder.Build();
        }
    }
}