﻿using Autofac;
using LessonNine.CSV;
using LessonNine.Infrastructure;

namespace LessonNine
{
    class Program
    {
        static void Main(string[] args)
        {
            AutofacInstaller.Register();

            var csvParser = AutofacInstaller.Container.Resolve<ICsvFileReader>();
        }
    }
}
