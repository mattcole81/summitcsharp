﻿    using System;
using System.Collections.Generic;
using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
using System.Threading.Tasks;
    using LessonSix.ClaimsChecking;

namespace LessonSix
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Implement the code below to return an ICNZClaimsChecker if the policy number starts with YN 
             and a VedaClaimsChecker if not.  These claims checkers should use the ICNZProxy and 
             VedaProxy classes internally to return a number of claims.
             */
            var policyNumber = "OA123";

            var claimsHistoryChecker = ClaimsHistoryCheckerFactory.GetClaimsHistoryChecker(policyNumber);
            var numberOfClaims = claimsHistoryChecker.GetNumberOfPreviousClaims();


            Console.WriteLine($"Policy: {policyNumber} has {numberOfClaims} previous claims.");
            Console.ReadLine();
        }
    }
}
