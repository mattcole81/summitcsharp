namespace LessonSix.ClaimsChecking
{
    public interface IClaimsHistoryChecker
    {
        int GetNumberOfPreviousClaims();
    }
}