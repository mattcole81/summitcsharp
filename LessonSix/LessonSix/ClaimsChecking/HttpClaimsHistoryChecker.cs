using LessonSix.Http;

namespace LessonSix.ClaimsChecking
{
    public abstract class HttpClaimsHistoryChecker : IClaimsHistoryChecker
    {
        private IHttpClient _httpClient = new DummyHttpClient();

        protected abstract string _GetUrl();
        protected abstract int _ParseResult(string response);

        public int GetNumberOfPreviousClaims()
        {
            var url = _GetUrl();
            var response = _httpClient.PerformRequest(url);
            return _ParseResult(response);
        }
    }
}