namespace LessonSix.Http
{
    public class DummyHttpClient : IHttpClient
    {
        public string PerformRequest(string url)
        {
            return url.Contains("veda") ? "{claims: 10}" : "<xml><claims>10</claims></xml>";
        }
    }
}