using System;
using System.Collections.Generic;

namespace LessonSix.Http
{
    public interface IHttpClient
    {
        string PerformRequest(string url);
    }


    public class PremiumCalculator
    {
        public double CalculatePremium(Dictionary<string, string> values)
        {
            // let's pretend I've validated the keys
            var premium = 10.0;
            if (Convert.ToInt32(values["number_of_previous_claims"]) > 3) premium += 4.2;
            premium += _GetValueForCar(values["car_model_key"]);

            if (Convert.ToInt32(values["number_drivers"]) > 2 && Convert.ToInt32(values["min_age_of_driver"]) < 20) premium += 34.4;

            return premium;
        }

        private double _GetValueForCar(string carModelKey)
        {
            if (carModelKey == "porsche.boxster") return 15.2;
            if (carModelKey == "holden.commodore") return 24.0;

            return 5;
        }
    }
}