﻿namespace LessonEight
{
    public class MoneyManger : IMoneyManager
    {
        private ICarExpense _carExpense;
        private IHomeExpense _homeExpense;

        public MoneyManger(ICarExpense carExpense, IHomeExpense homeExpense)
        {
            _carExpense = carExpense;
            _homeExpense = homeExpense;
        }

        public decimal GetTotalCost()
        {
            return _carExpense.GetCost() + _homeExpense.GetCost();
        }
    }
}