namespace LessonEight
{
    public class CarExpense : ICarExpense
    {
        private ITyreExpense _tyreExpense;
        private IServiceExpense _serviceExpense;

        public CarExpense(ITyreExpense tyreExpense, IServiceExpense serviceExpense)
        {
            _tyreExpense = tyreExpense;
            _serviceExpense = serviceExpense;
        }

        public decimal GetCost()
        {
            return 6 + _tyreExpense.GetCost() + _serviceExpense.GetCost();
        }
    }
}