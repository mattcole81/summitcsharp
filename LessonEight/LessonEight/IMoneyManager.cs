﻿namespace LessonEight
{
    public interface IMoneyManager
    {
        decimal GetTotalCost();
    }
}