namespace LessonEight
{
    public interface IServiceExpense
    {
        decimal GetCost();
    }
}