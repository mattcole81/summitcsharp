﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace LessonEight
{
    public class AutofacInstaller
    {
        public static IContainer Container
        {
            get;
            set;
        }

        public static void Register()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<CarExpense>().As<ICarExpense>();
            builder.RegisterType<HomeExpense>().As<IHomeExpense>();
            builder.RegisterType<TyreExpense>().As<ITyreExpense>();
            builder.RegisterType<ServiceExpense>().As<IServiceExpense>();
            builder.RegisterType<MoneyManger>().As<IMoneyManager>();

            builder.RegisterType<FillerExpense>();
            builder.RegisterType<FillerType>();

            Container = builder.Build();
        }
    }
}
