﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Topshelf;

namespace LessonEight
{
    class Program
    {
        static void Main(string[] args)
        {
            AutofacInstaller.Register();

            var money = AutofacInstaller.Container.Resolve<IMoneyManager>();

            Console.WriteLine("Total Expense is " + money.GetTotalCost());
            Console.ReadKey();
        }
    }
}
