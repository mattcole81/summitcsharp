﻿namespace LessonEight
{
    public interface IHomeExpense
    {
        decimal GetCost();
    }
}