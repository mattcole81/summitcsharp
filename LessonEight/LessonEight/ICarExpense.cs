﻿namespace LessonEight
{
    public interface ICarExpense
    {
        decimal GetCost();
    }
}