namespace LessonEight
{
    public interface ITyreExpense
    {
        decimal GetCost();
    }
}