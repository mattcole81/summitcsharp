﻿using System;

namespace LessonTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Problem one*/
            // Write a method that takes a string and returns the length of the string, and as an out parameter the number of times the letter 'e' appears in the string
            // print the result to screen
            // e.g for the string 'elephant' the printed result should be elephant: 8 chars, 2 e's

            /* Problem Two */
            // print the number of words and number of characters in the strings below
            // .Split is a method that will split a string on a character and return an array containing the parts of the string that have been split
            // .Length tells you the number of characters in a string
            var stringOne = "The quick brown fox";
            var stringTwo = "jumped over the lazy dog";

            // output to screen should look like:
            // The quick brown fox: 4 words, 19 chars
            // jumped over the lazy dog: 5 words, 24 chars

            // Problem Three
            /* Write a program that will take an base rate, calculate a premium (by multiplying the base rate by 1.8), then apply a discount
             * of 15% and return a final value. Make each operation it's own method and allow the base rate to be supplied to the first method as 
             * a parameter             
             */
            
            Console.ReadLine();

            /* Problem Four */
            // refactor this code to use methods to avoid repetition
            Action<string,char> _LookForLetter = (word, letterToLookFor) =>
            {
                foreach (var letter in word.ToLower())
                {
                    Console.WriteLine(letter == letterToLookFor ? "Found an A!" : $"Dammit no A, was a {letter}");
                }
            };
            _LookForLetter("douglas", 'a');
            _LookForLetter("inman", 'a');
        }

        private static void _LookForLetter(string word, char letterToLookFor)
        {
            foreach (var letter in word.ToLower())
            {
                Console.WriteLine(letter == letterToLookFor ? "Found an A!" : $"Dammit no A, was a {letter}");
            }
        }
    }
}
