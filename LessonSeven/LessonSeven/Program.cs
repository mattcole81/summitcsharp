﻿using System;
using System.Security.Cryptography.X509Certificates;
using LessonSeven.MockService;
using LessonSeven.Service;

namespace LessonSeven
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Complete the program to output the premium value calculated from each of the insurance providers.
             * 
             * A provider may not offer insurance for certain risk types.
             * 
             * Implement the QuoteService to return a list of premiums from each of the applicable service providers.
             * 
             * Implement the InsuranceProviderFactory to return a list of providers who offer the appropriate insurance.
             * 
             * Extend the insurance providers to offer Caravan insurance
             */
            
            var clientInformation = ClientForm.CaptureClientInformation();

            var quoteService = new QuoteService();

            var premium = quoteService.GetPremium(clientInformation);

            var prem = premium.Count;

            Console.WriteLine($"{clientInformation.FirstName} {clientInformation.Surname} your quotes are : ");
            Console.WriteLine();

            foreach (var item in premium)
            {
                Console.WriteLine($"{item.ProviderName} will offer you a premium of {item.Premium.ToString("C")}");
            }

            Console.ReadKey();
        }
    }
}
