using System;

namespace LessonSeven
{
    public class ClientInformation : IClientInformation
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public RiskType RiskType  { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; }
    }
}