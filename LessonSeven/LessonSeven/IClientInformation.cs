namespace LessonSeven
{
    public interface IClientInformation
    {
        int Age { get; }

        RiskType RiskType { get; }
    }
}