﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using LessonSeven.InsuranceProvider.Providers;

namespace LessonSeven.InsuranceProvider
{
    public class InsuranceProviderFactory
    {
        public static List<IInsuranceProvider> GetInsuranceProviders()
        {
            var returnList = new List<IInsuranceProvider>();

            var aamiProvider = new AamiQuoteProvider();
            var youiProvider = new YouiQuoteProvider();
            var suncorpProvider = new SuncorpQuoteProvider();

            returnList.Add(aamiProvider);
            returnList.Add(youiProvider);
            returnList.Add(suncorpProvider);

            return returnList;
        }
    }
}
