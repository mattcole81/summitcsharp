﻿using LessonSeven.MockService;

namespace LessonSeven.InsuranceProvider.Providers
{
    public class SuncorpQuoteProvider : IInsuranceProvider
    {
        public string Name { get { return "Suncorp"; } }

        public decimal GetHomePremium(IClientInformation clientInformation)
        {
            return DummyPremiumProvider.CalculatePremium(clientInformation);
        }

        public decimal GetPremium(IClientInformation clientInformation)
        {
            switch (clientInformation.RiskType)
            {
                case RiskType.Home:
                    return GetHomePremium(clientInformation);
                    break;
                case RiskType.Vehicle:
                    return GetVehiclePremium(clientInformation);
                    break;
            }

            return -1;
        }

        public decimal GetVehiclePremium(IClientInformation clientInformation)
        {
            return DummyPremiumProvider.CalculatePremium(clientInformation);
        }
    }
}