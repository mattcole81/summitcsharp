namespace LessonSeven.InsuranceProvider.Providers
{
    public interface IInsuranceProvider
    {
        string Name { get; }

        decimal GetVehiclePremium(IClientInformation clientInformation);

        decimal GetHomePremium(IClientInformation clientInformation);

        decimal GetPremium(IClientInformation clientInformation);
    }
}