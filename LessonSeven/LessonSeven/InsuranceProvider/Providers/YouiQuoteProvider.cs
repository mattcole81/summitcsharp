﻿using LessonSeven.MockService;

namespace LessonSeven.InsuranceProvider.Providers
{
    public class YouiQuoteProvider : IInsuranceProvider
    {
        public string Name { get { return "Youi"; } }

        public decimal GetVehiclePremium(IClientInformation clientInformation)
        {
            return DummyPremiumProvider.CalculatePremium(clientInformation);
        }

        public decimal GetHomePremium(IClientInformation clientInformation)
        {
            return DummyPremiumProvider.CalculatePremium(clientInformation);
        }

        public decimal GetPremium(IClientInformation clientInformation)
        {
            switch (clientInformation.RiskType)
            {
                case RiskType.Home:
                    return GetHomePremium(clientInformation);
                    break;
                case RiskType.Vehicle:
                    return GetVehiclePremium(clientInformation);
                    break;
            }

            return -1;
        }
    }
}
