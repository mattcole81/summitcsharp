﻿using LessonSeven.MockService;

namespace LessonSeven.InsuranceProvider.Providers
{
    public class AamiQuoteProvider : IInsuranceProvider
    {
        public string Name { get { return "AAMI"; } }

        public decimal GetVehiclePremium(IClientInformation clientInformation)
        {
            return AamiPremiumCalculator.CalculatePremium(clientInformation);
        }

        public decimal GetHomePremium(IClientInformation clientInformation)
        {
            return AamiPremiumCalculator.CalculatePremium(clientInformation);
        }

        public decimal GetPremium(IClientInformation clientInformation)
        {
            switch (clientInformation.RiskType)
            {
                case RiskType.Home:
                    return GetHomePremium(clientInformation);
                    break;
                case RiskType.Vehicle:
                    return GetVehiclePremium(clientInformation);
                    break;
            }

            return -1;
        }
    }
}