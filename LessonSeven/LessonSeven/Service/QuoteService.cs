﻿using System.Collections.Generic;
using LessonSeven.InsuranceProvider;
using LessonSeven.InsuranceProvider.Providers;


namespace LessonSeven.Service
{
    public class QuoteService : IQuoteService
    {
        public List<PremiumResults> GetPremium(ClientInformation clientInformation)
        {
            var mylist = new List<PremiumResults>();
            var providerList = InsuranceProviderFactory.GetInsuranceProviders();

            foreach (var provider in providerList)
            {
                var premiumResult = new PremiumResults()
                {
                    Premium = provider.GetPremium(clientInformation),
                    ProviderName = provider.Name
                };
                mylist.Add(premiumResult);
            }

            return mylist;
        }
    }
}