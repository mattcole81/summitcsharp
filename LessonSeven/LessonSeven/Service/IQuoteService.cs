using System.Collections.Generic;

namespace LessonSeven.Service
{
    public interface IQuoteService
    {
        List<PremiumResults> GetPremium(ClientInformation clientInformation);
    }
}