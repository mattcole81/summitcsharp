﻿namespace LessonSeven.Service
{
    public class PremiumResults
    {
        public string ProviderName { get; set; }
        public decimal Premium { get; set; }
    }
}