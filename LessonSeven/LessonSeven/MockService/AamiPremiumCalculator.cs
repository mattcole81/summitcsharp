using System;

namespace LessonSeven.MockService
{
    public class AamiPremiumCalculator
    {
        public static decimal CalculatePremium(IClientInformation clientInformation)
        {
            var premiumfactor = clientInformation.Age > 25 ? 20 : 80;

            return new Random().Next(20, 300) + premiumfactor;
        }
    }
}