using System;

namespace LessonSeven
{
    public class ClientForm
    {
        public static ClientInformation CaptureClientInformation()
        {
            Console.Write("First Name                    ");
            var firstName = Console.ReadLine();
            Console.Write("Surname                       ");
            var surname = Console.ReadLine();
            Console.Write("Date Of Birth (dd/MM/yyyy)    ");
            var dob = Console.ReadLine();
            Console.Write("Risk Type (V)ehicle or (H)ome ");
            var riskType = Console.ReadLine();

            return new ClientInformation()
            {
                FirstName = firstName,
                RiskType = riskType.ToUpper().Equals("V") ? RiskType.Vehicle : RiskType.Home,
                DateOfBirth = Convert.ToDateTime(dob),
                Surname = surname
            };
        }
    }
}