﻿namespace LessonThree
{
    public class PricingRule
    {
        public PolicyType PolicyType { get; private set; }
        public double Price { get; private set; }

        public PricingRule(PolicyType policyType, double price)
        {
            PolicyType = policyType;
            Price = price;
        }
    }
}