﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LessonThree
{
    public class Claim
    {
        private readonly PolicyType _policyType;
        private List<ClaimItem> _claimItems = new List<ClaimItem>();

        public Claim(PolicyType policyType)
        {
            _policyType = policyType;
        }

        public void AddItem(string description, double value)
        {
            var claimItem = new ClaimItem(description, value);
            _claimItems.Add(claimItem);
        }

        public double GetClaimTotal()
        {
            return _claimItems.Sum(x => x.Value);
        }
    }
}