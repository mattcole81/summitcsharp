﻿using System;

namespace LessonThree
{
    public class PremiumCalculator
    {
        private readonly double _homePrice;
        private double _vehiclePrice;

        public PremiumCalculator(double homePrice, double vehiclePrice)
        {
            _homePrice = homePrice;
            _vehiclePrice = vehiclePrice;
        }

        public void UpdateVehiclePrice(double vehiclePrice)
        {
            _vehiclePrice = vehiclePrice;
        }

        public double Calculate(Policy policy)
        {
            if(policy == null) throw new ArgumentException("Must supply a non null policy");
            
            switch (policy.PolicyType)
            {
                case PolicyType.Home:
                    return _homePrice;
                case PolicyType.Vehicle:
                    return _vehiclePrice;
            }
            return 100.0;
        }
    }
}