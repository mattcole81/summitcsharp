﻿namespace LessonThree
{
    public enum PolicyType
    {
        Vehicle,
        Home,
        Watercraft,
        BusinessLiability
    }
}