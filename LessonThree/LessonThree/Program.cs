﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonThree
{
    class Program
    {
        static void Main(string[] args)
        {
            //var calculator = new PremiumCalculator(23.4, 12.2);
            //calculator.UpdateVehiclePrice(28.4);

            //var vehPolicy = new Policy(PolicyType.Vehicle);

            //var premium = calculator.Calculate(vehPolicy);

            Console.WriteLine(premium);
            Console.ReadLine();
        }
    }
    /*     
    1. Create the ability to set a price for vehicle policies and home policies when creating a PremiumCalculator.  These should be double values
     and only visible inside the PremiumCalculator.
          
    2. Write an implementation for the Calculate method that takes a PolicyType as a parameter and returns the vehicle price if it is a vehicle
     policy, the home price if it's a home policy or 100.0 if it's a different type.  Create an instance of the PremiumCalculator and call this method
     in the Main method above.
     
    3. Write a method that allows a client of the PremiumCalculator to update the price for vehicle policies.  Call this method with a new price.
     
    4. Create a policy class that requires a policy type when being created and stores the policy type in a way that is publically visible by clients
     of the class.
     
    5.  Modify the method you wrote in 2. to take an instance of your Policy class defined in 4 instead of the PolicyType variable but returns the same results.  
     Call this method.
 
    6.  ADVANCED. Modify the PremiumCalculator to have no knowledge of pricing.  Instead, define a PricingRule class that takes a policy type and a price
     when being created and exposes a single method Calculate that is passed a PolicyType argument and if it matches the policy type supplied to the PricingRule, 
     it should return the price supplied, if not it should return 0.  Pass an array of these objects to the PremiumCalculator when creating it and use the array to
     calculate the price when Calculate is called on the PremiumCalculator.
    
    7.  Create a new class called Claim, which will accept a PolicyType as a parameter when it's being created, and store
    it.

    8. Create another class called ClaimItem which requires a description and a value when being created.

    9.  Create the ability to add ClaimItems to a Claim and to store them.

    10.  Create a method on the Claim class to calculate and return the total value of the ClaimItems on the claim.


     *  */

    
}
