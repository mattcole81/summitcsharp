﻿namespace LessonThree
{
    public class ClaimItem
    {
        private readonly string _description;
        private readonly double _value;

        public ClaimItem(string description, double value)
        {
            _description = description;
            _value = value;
        }
    }
}