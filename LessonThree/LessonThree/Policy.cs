﻿namespace LessonThree
{
    public class Policy
    {
        public PolicyType PolicyType { get; private set; }

        public Policy(PolicyType type)
        {
            PolicyType = type;
        }
    }
}