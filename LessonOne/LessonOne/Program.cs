﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOne
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Console.Write("some value"); // this writes to the screen
             string input = Console.ReadLine(); // this reads input from the user
            
             */
           // _PrintOneToTen();
            //_SumOfMultiplesBelow1000();

            //_HowManyNumbersAreDivisibleByThree();

            _DivisionCalculator();

            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static void _PrintOneToTen()
        {
            // display the numbers 1 to 10 on screen, using "while".
            var count = 1;
            while (count <= 10)
            {
                Console.WriteLine(count);
                count++;
            }
        }

        private static void _SumOfMultiplesBelow1000()
        {
            //Find the sum of all the multiples of 3 or 5 below 1000
            //var sum = Enumerable.Range(1, 1000)
            //    .Where(i => i%3 == 0 || i%5 == 0)
            //    .Sum(i => i);
            var sum = 0;
            for (var count = 1; count <= 1000; count++)
            {
                sum += (count % 3 == 0 || count % 5 == 0) ? count : 0;
            }
            Console.WriteLine(sum);
        }

        private static void _HowManyNumbersAreDivisibleByThree()
        {
            //Write a program to count how many numbers between 1 and 1000 are divisible by 3 with no remainder
            // in C# % is the mod operator
            // int val = 5%3 will assign 2 to val

            var count = 0;
            for (int i = 1; i <= 1000; i++)
            {
                if (i%3 == 0)
                {
                    count++;
                }
            }
        }

        private static void _DivisionCalculator()
        {
            // Ask the user for two numbers, and show their division if the second number is not zero; otherwise, it will display "I cannot divide by zero"
            bool haveInt;
            var numberOne = _AskAndGetNumber("Enter number one:");
            var numberTwo = _AskAndGetNumber("Enter number two:");
          
            if (numberTwo == 0)
            {
                Console.WriteLine("Can't divide by zero");
                return;
            }
            var result = numberOne/numberTwo;
            Console.WriteLine($"The result was: {result}");
        }

        private static int _AskAndGetNumber(string messageToShow)
        {
            Console.WriteLine(messageToShow);
            var numberFromUser = 0;
            var haveInt = false;
            while (!haveInt)
            {
                haveInt = _GetIntFromConsole(out numberFromUser);
            }
            return numberFromUser;
        }

        private static bool _GetIntFromConsole(out int intValue)
        {
            var numberOneAsString = Console.ReadLine();
            var parseSuccess = int.TryParse(numberOneAsString, out intValue);
            if (!parseSuccess) Console.WriteLine("Dude that's not a number, please try again");
            return parseSuccess;
        }
    }
}
